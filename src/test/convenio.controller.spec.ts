import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import { BoletoService } from '../boleto/boleto.service'
import { AppModule } from '../app.module'
import * as request from 'supertest'

describe('Pagamento de Concessionárias', () => {
  let app: INestApplication
  let service: BoletoService

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(BoletoService)
      .useValue(service)
      .compile()

    app = module.createNestApplication();
    await app.init();
  })

  it('/GET boleto/{numero} - número válido', () => {
    const numero = '82660000000794700422019072610000019012695252'
    const mock = {"valor":79.47,"vencimento":"2019-7-26","numero_boleto":"82660000000794700422019072610000019012695252"}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(201)
      .expect(mock)
  })

  it('/GET boleto/{numero} - sem vencimento', () => {
    const numero = '82680000000794700420000000010000019012695252'
    const mock = {"valor":79.47,"numero_boleto":"82680000000794700420000000010000019012695252"}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(201)
      .expect(mock)
  })

  it('/GET boleto/{numero} - número inválido', () => {
    const numero = '826600000007947004220190726100000190126952521'
    const mock = {"erro":"Código de barras inválido.","valor":"826600000007947004220190726100000190126952521"}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(400)
      .expect(mock)
  })

  it('/GET boleto/{numero} - dígito inválido', () => {
    const numero = '82660000000794700422019072610000019012695250'
    const mock = {"erro":"Dígito verificador inválido.","valor":6}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(400)
      .expect(mock)
  })

})
