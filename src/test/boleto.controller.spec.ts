import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import { BoletoService } from '../boleto/boleto.service'
import { AppModule } from '../app.module'
import * as request from 'supertest'

describe('Testes Gerais', () => {
  let app: INestApplication
  let service: BoletoService

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(BoletoService)
      .useValue(service)
      .compile()

    app = module.createNestApplication();
    await app.init();
  })

  it('/GET boleto/{numero} - requisição inválida', () => {
    return request(app.getHttpServer())
      .get(`/boleto`)
      .expect(404)
  })

  it('/GET boleto/{numero} - acesso a outras urls (1)', () => {
    return request(app.getHttpServer())
      .get(`/outraurl`)
      .expect(404)
  })

  it('/GET boleto/{numero} - acesso a outras urls (2)', () => {
    return request(app.getHttpServer())
      .get(`/`)
      .expect(404)
  })

  it('/GET boleto/{numero} - entrada inválida (1)', () => {
    const numero = '18948948979878798499819819846548594658125566'
    const mock = {"erro":"Código de barras inválido.","valor":'18948948979878798499819819846548594658125566'}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(400)
      .expect(mock)
  })

  it('/GET boleto/{numero} - entrada inválida (2)', () => {
    const numero = 'saldmqwpfomqwpoasdfelkmjnhklijiojhkslgkckkkk'
    const mock = {"erro":"Código de barras inválido.","valor":'saldmqwpfomqwpoasdfelkmjnhklijiojhkslgkckkkk'}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(400)
      .expect(mock)
  })

  it('/GET boleto/{numero} - entrada inválida (3)', () => {
    const numero = '189489489798787984998198198465415616548594658125566'
    const mock = {"erro":"Código de barras inválido.","valor":'189489489798787984998198198465415616548594658125566'}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(400)
      .expect(mock)
  })

  it('/GET boleto/{numero} - entrada inválida (4)', () => {
    const numero = '8979878798499819819846548594658125566'
    const mock = {"erro":"Código de barras inválido.","valor":'8979878798499819819846548594658125566'}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(400)
      .expect(mock)
  })

})
