import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import { BoletoService } from '../boleto/boleto.service'
import { AppModule } from '../app.module'
import * as request from 'supertest'

describe('Título Bancário', () => {
  let app: INestApplication
  let service: BoletoService

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(BoletoService)
      .useValue(service)
      .compile()

    app = module.createNestApplication();
    await app.init();
  })

  it('/GET boleto/{numero} - número válido', () => {
    const numero = '00193373700000001000500940144816060680935031'
    const mock = {"valor":1,"vencimento":"2007-12-31","numero_boleto":"00193373700000001000500940144816060680935031"}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(201)
      .expect(mock)
  })

  it('/GET boleto/{numero} - sem vencimento', () => {
    const numero = '00198000000000001000500940144816060680935031'
    const mock = {"valor":1,"numero_boleto":"00198000000000001000500940144816060680935031"}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(201)
      .expect(mock)
  })

  it('/GET boleto/{numero} - número inválido', () => {
    const numero = '001933737000000010005009401448160606809350311'
    const mock = {"erro":"Código de barras inválido.","valor":"001933737000000010005009401448160606809350311"}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(400)
      .expect(mock)
  })

  it('/GET boleto/{numero} - dígito inválido', () => {
    const numero = '00193373700000001000500940144816060680935032'
    const mock = {"erro":"Dígito verificador inválido.","valor":3}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(400)
      .expect(mock)
  })

  it('/GET boleto/{numero} - entrada inválida (1)', () => {
    const numero = '18948948979878798499819819846548594658125566'
    const mock = {"erro":"Código de barras inválido.","valor":'18948948979878798499819819846548594658125566'}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(400)
      .expect(mock)
  })

  it('/GET boleto/{numero} - entrada inválida (2)', () => {
    const numero = 'saldmqwpfomqwpoasdfelkmjnhklijiojhkslgkckkkk'
    const mock = {"erro":"Código de barras inválido.","valor":'saldmqwpfomqwpoasdfelkmjnhklijiojhkslgkckkkk'}

    return request(app.getHttpServer())
      .get(`/boleto/${numero}`)
      .expect(400)
      .expect(mock)
  })
})
