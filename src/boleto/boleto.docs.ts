import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'

export const setUpDocs = (app) => {
  const options = new DocumentBuilder()
    .setTitle('Exemplo de Utilização')
    .setDescription('API de identificação de boletos.')
    .setVersion('1.0.0')
    .build()

  const document = SwaggerModule.createDocument(app, options)
  SwaggerModule.setup('documentacao', app, document)
}
