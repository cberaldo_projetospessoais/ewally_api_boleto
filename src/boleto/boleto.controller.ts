import { Controller, Get, Res, Param, HttpStatus, Logger } from '@nestjs/common'
import { BoletoService } from './boleto.service'
import { ApiUseTags, ApiImplicitParam, ApiBadRequestResponse, ApiCreatedResponse } from '@nestjs/swagger';
import Boleto from './boleto.model'

@Controller('boleto')
@ApiUseTags('boleto')
export class BoletoController {
  // injeção de dependências do NestJS.
  constructor(private readonly boletoService: BoletoService) {}

  @Get('/:numero')
  @ApiImplicitParam({ name: 'numero' })
  @ApiCreatedResponse({ description: 'Código de barras validado e informações foram recuperadas.' })
  @ApiBadRequestResponse({ description: 'Requisição inválida ou erro na entrada de dados.' })
  getBoleto(@Res() res, @Param('numero') numero) {
    this.boletoService
      .identificarTipoDeBoleto(numero)
      .then((boleto: Boleto) => (res)
        .status(HttpStatus.CREATED)
        .json({
          valor: boleto.valor,
          vencimento: boleto.vencimento && boleto.vencimento.toLocaleDateString(),
          numero_boleto: boleto.numero_boleto
        })
      )
      .catch((erro) => (res)
        .status(HttpStatus.BAD_REQUEST)
        .json(erro)
      )
  }
}
