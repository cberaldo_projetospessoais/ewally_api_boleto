import CustomError from '../models/error.model'

export class BoletoError extends CustomError {
}

export interface IBoleto {
  readonly numero_boleto: string
  readonly vencimento: Date
  readonly valor: number
  readonly digito_verificador: number

  calcularDigitoVerificador(index: number)
  calcularValor()
  calcularVencimento()
  ajustarCodigoDeBarras()

  validarDigitoVerificador(): Boolean
  validarCodigoDeBarras(): Boolean

  digitoVerificadorIndex(): number
  digitoVerificadorInformado(index: number): number
}

export default abstract class Boleto implements IBoleto {
  numero_boleto: string
  valor: number
  vencimento: Date
  digito_verificador: number
  
  calcularDigitoVerificador = null
  calcularVencimento = null
  ajustarCodigoDeBarras = null

  private valor_range: number[]
  private digito_verificador_index: number

  calcularValor = () => {
    const valor_array = this.numero_boleto.slice(this.valor_range[0], this.valor_range[1]).split('')
    valor_array.splice(valor_array.length -2, 0, '.') // adicionar separador de casa decimal.
    this.valor = Number(valor_array.join(''))
  }

  validarDigitoVerificador = () => this.digito_verificador === this.digitoVerificadorInformado()
  validarCodigoDeBarras = () => !isNaN(Number(this.numero_boleto)) && this.numero_boleto.length === 44

  digitoVerificadorInformado = () => Number(this.numero_boleto.substr(this.digito_verificador_index, 1))
  digitoVerificadorIndex = () => this.digito_verificador_index

  constructor(numero: string, digito_index: number, valor_range: number[]) {
    this.numero_boleto = numero
    this.digito_verificador_index = digito_index
    this.valor_range = valor_range
  }
}
