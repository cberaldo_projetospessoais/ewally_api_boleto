import { Injectable, Logger } from '@nestjs/common'
import { BoletoError } from './boleto.model'
import Convenio from '../models/convenio.model'
import Titulo from '../models/titulo.model'
import Boleto from './boleto.model'

@Injectable()
export class BoletoService {
  identificarTipoDeBoleto (numero) : Promise<Boleto> {
    return new Promise<Boleto>((resolve, reject) => {
      try {
        const testValues = (regexArray, valor) => !regexArray.every((regex) => !regex.test(valor))
        const boletoPagamentoConcessionaria = [/8\d{1}\d{1}[6-9]{1}\d{40}\b/g, /8\d{1}\d{1}[6-9]{1}\d{44}\b/g]
        const boletoTituloBancario = [/0019\d{40}\b/g, /0019\d{43}\b/g]

        let boleto

        if (testValues(boletoPagamentoConcessionaria, numero)) {
          boleto = new Convenio(numero)
        }

        if (testValues(boletoTituloBancario, numero)) {
          boleto = new Titulo(numero)
        }

        if (!boleto) {
          reject(new BoletoError('Código de barras inválido.', numero))
          return
        }

        if (!boleto.validarCodigoDeBarras()) {
          reject(new BoletoError('Código de barras inválido.', boleto.numero_boleto))
          return
        }

        if (!boleto.validarDigitoVerificador()) {
          reject(new BoletoError('Dígito verificador inválido.', boleto.digitoVerificadorInformado()))
          return
        }

        resolve(boleto)
      } catch (error) {
        Logger.error(error.message, 'Erro interno')
        reject({ erro: 'Erro interno.' })
      }
    })
  }
}
