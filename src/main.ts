import { NestFactory } from '@nestjs/core'
import { Logger } from '@nestjs/common'
import { AppModule } from './app.module'
import { setUpDocs } from './boleto/boleto.docs'
import { json } from 'body-parser'

async function bootstrap() {
  const port = process.env.PORT || 3000

  const app = await NestFactory.create(AppModule)

  setUpDocs(app)

  app.use(json())
  app.enableCors()

  app.listen(port, () => {
    Logger.log(`http://localhost:${port}`, 'Server listenning on port')
    Logger.log(`http://localhost:${port}/documentacao`, `Domentation at`)
  })
}

bootstrap()
