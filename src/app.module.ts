import { Module } from '@nestjs/common'
import { BoletoController } from './boleto/boleto.controller'
import { BoletoService } from './boleto/boleto.service'

@Module({
  imports: [],
  controllers: [BoletoController],
  providers: [BoletoService],
})
export class AppModule {}
