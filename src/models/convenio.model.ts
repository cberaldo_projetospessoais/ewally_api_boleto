import Boleto from '../boleto/boleto.model'

export default class Convenio extends Boleto {

  constructor (numero) {
    super(numero, 3, [5, 15])
    this.ajustarCodigoDeBarras()
    this.calcularDigitoVerificador()
    this.calcularVencimento()
    this.calcularValor()
  }

  calcularVencimento = () => {
    const vencimento = this.numero_boleto.substring(19, 27).split('')
    const vencimentoString = []
      .concat(vencimento.slice(4, 6))
      .concat('/', vencimento.slice(6, 8))
      .concat('/', vencimento.slice(0, 4))
      .join('')

    this.vencimento = new Date(vencimentoString)

    if (isNaN(this.vencimento.getTime())) {
      this.vencimento = undefined
    }
  }

  calcularDigitoVerificador = () => {
    var codigo = this.numero_boleto.split('')
    var multiplicador = [2, 1]

    // zerar quinta posição para não considerar no cálculo do dígito verificador.
    codigo.splice(this.digitoVerificadorIndex(), 1)
    
    var somatoria = codigo
      .reverse()
      .map((digito, index) =>
        String(multiplicador[index % multiplicador.length] * Number(digito)).split(''))
      .reduce((array, digitos) => array.concat(digitos), [])
      .map((digito) => Number(digito))
      .reduce((previousValue, currentValue) => previousValue + currentValue)

    var digito = (somatoria % 10)

    if (digito === 0) {
      this.digito_verificador = 0
      return
    }

    this.digito_verificador = 10 - digito
  }

  ajustarCodigoDeBarras = () => {
    if (this.numero_boleto.length !== 48)
      return

    const test: string[] = this.numero_boleto.split('')
    test.splice(47, 1)
    test.splice(35, 1)
    test.splice(23, 1)
    test.splice(12, 1)
    this.numero_boleto = test.join('')
  }
}