
export interface IBoleto {
  readonly identificacao: Array<RegExp>
  readonly numero_boleto: string
  readonly vencimento: Date
  
  readonly valor: number
  readonly valor_range: number[]

  readonly digito_verificador: number
  readonly digito_verificador_index: number

  calcularDigitoVerificador()
  calcularValor()
  calcularVencimento()

  validarDigitoVerificador(): Boolean
  validarCodigoDeBarras(): Boolean
  digitoVerificadorInformado(): number
}

abstract class Boleto implements IBoleto {
  identificacao: Array<RegExp>
  numero_boleto: string
  valor: number
  vencimento: Date
  digito_verificador: number
  digito_verificador_index: number
  valor_range: number[]

  calcularDigitoVerificador = null
  calcularVencimento = null

  calcularValor = () => {
    const valor_array = this.numero_boleto.slice(this.valor_range[0], this.valor_range[1]).split('')
    valor_array.splice(valor_array.length -2, 0, '.') // adicionar separador de casa decimal.
    this.valor = Number(valor_array.join(''))
  }

  digitoVerificadorInformado = () => Number(this.numero_boleto.substr(this.digito_verificador_index, 1))
  validarDigitoVerificador = () => this.digito_verificador === this.digitoVerificadorInformado()
  validarCodigoDeBarras = () => !this.identificacao.every(regex => !regex.test(this.numero_boleto))

  constructor (numero) {
    this.numero_boleto = numero
  }
}

export default Boleto