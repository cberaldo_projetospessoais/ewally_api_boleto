
export default class CustomError extends Error {
  erro: string
  valor: string

  constructor(mensagem, valor) {
    super(mensagem)
    this.erro = mensagem
    this.valor = valor
  }
}
