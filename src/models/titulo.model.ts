import Boleto from '../boleto/boleto.model'

export default class Titulo extends Boleto {

  constructor (numero) {
    super(numero, 4, [9, 19])
    this.ajustarCodigoDeBarras()
    this.calcularDigitoVerificador()
    this.calcularVencimento()
    this.calcularValor()
  }

  calcularVencimento = () => {
    const somarData = (data, fator) : Date => new Date(Number(data) + (fator * 24 * 60 * 60 * 1000)) // calcula data em milissegundos
    const calcularDiferenca = (data1, data2) : number => Math.floor((((Number(data1) - Number(data2)) / 24 / 60 / 60 / 1000) / 9999)) // verifica quantas "viradas" se passaram desde a data fixada.

    const dataAtual = new Date()
    // data "Fixada" conforme documento de layout.
    const dataFixada = new Date('10/07/1997 23:59')
    const fatorVencimento = Number(this.numero_boleto.slice(5, 9))

    // se o fator de vencimento não estiver entre o valor estipulado.
    if (!(fatorVencimento >= 1000 && fatorVencimento <= 9999)) {
      this.vencimento = undefined
      return
    }

    const diferenca = calcularDiferenca(dataAtual, dataFixada)
    // calcular automaticamente as "viradas" de data (Ex.: 21/02/2025 começa a contar de 1000 novamente).
    const data_corrigida = somarData(dataFixada, diferenca * 9999)

    // retorna a data de vencimento do boleto.
    this.vencimento = somarData(data_corrigida, fatorVencimento)
  }

  calcularDigitoVerificador = () => {
    var codigo = this.numero_boleto.split('')
    var multiplicador = [2, 3, 4, 5, 6, 7, 8, 9]

    // zerar quinta posição para não considerar no cálculo do dígito verificador.
    codigo.splice(this.digitoVerificadorIndex(), 1)

    var somatoria = codigo
      .reverse()
      .map((digito, index) => multiplicador[(index % multiplicador.length)] * Number(digito))
      .reduce((previousValue, currentValue) => previousValue + currentValue)

    var digito = 11 - (somatoria % 11)

    if ([0, 10, 11].includes(digito)) {
      this.digito_verificador = 1
      return
    }

    this.digito_verificador = digito
  }

  ajustarCodigoDeBarras = () => {
    if (this.numero_boleto.length !== 47)
      return

    const test: string[] = this.numero_boleto.split('')
    test.splice(31, 1)
    test.splice(20, 1)
    test.splice(9, 1)
    this.numero_boleto = test.join('')
  }
}